
from enum import Enum
import Registers

class Special(Enum):
    DQUOTE_BEG = 1
    DQUOTE_END = 2
    SQUOTE_BEG = 3
    SQUOTE_END = 4
    RLIST_BEG = 5
    RLIST_END = 6
    ADDR_BEG = 7
    ADDR_END = 8
    COMMENT_BEG = 9
    DASH = 10
    COMMA = 11
    PLUS = 12
    EQUAL = 13
    COLON = 14

class State:    
    def __init__(self):
        self.in_comment = False
        self.in_squot = False
        self.in_dquot = False
        self.last_c = None

class StringLit:
    def __init__(self, s, until = None):
        if until is None:
            self.str = "".join(s)
        else:
            t = []
            for c in s:
                if c is until:
                    break
                else:
                    t.append(c)
            self.str = "".join(t)

class SQuote(StringLit):
    terminal = Special.SQUOTE_END
    def __init__(self, cg):
        super().__init__(cg, until = __class__.terminal)

    def __str__(self):
        return "SQuote: '{0}'".format(self.str)

class DQuote(StringLit):
    terminal = Special.DQUOTE_END
    def __init__(self, cg):
        super().__init__(cg, until = __class__.terminal)

    def __str__(self):
        return "DQuote: '{0}'".format(self.str)

class Comment(StringLit):
    def __init__(self, cg):
        super().__init__([c for c in cg])

    def __str__(self):
        return "Comment: '{0}'".format(self.str)

class Address:
    def __init__(self, tg):
        cur_tok = None
        prv_tok = None
        self.words = []
        while True:
            try:
                cur_tok = next(tg)
            except:
                raise RuntimeError("expected end of address token ']' before end of line")
            if cur_tok is Special.ADDR_END:
                break
            elif cur_tok is Special.PLUS:
                if len(self.words) == 1:
                    pass
                else:
                    raise RuntimeError('unexpected {0}'.format(cur_tok))
            elif len(self.words) == 0:
                self.words.append(cur_tok)
            elif len(self.words) == 1:
                if prv_tok is Special.PLUS:
                    self.words.append(cur_tok)
                else:
                    raise RuntimeError("expected '+'")
            else:
                raise RuntimeError('unexpected {0} while parsing address'.format(cur_tok))
            
            prv_tok = cur_tok

    def __str__(self):
        return "Address: {0}".format(self.words)

class SegmentAddress:
    def __init__(self, seg, addr):
        self.seg = seg
        self.addr = addr

    def __str__(self):
        return "SegmentAddress: {0}".format([self.seg] + self.addr.words)

class RegisterList:

    valid_regs = {
        'r0': 0,
        'r1': 1,
        'r2': 2,
        'r3': 3,
        'r4': 4,
        'sp': 5,
        'lr': 6,
        'pc': 7}

    inv_valid_regs = { val: key for (key, val) in valid_regs.items() }
    
    def __init__(self, tg):
        self.register_set = set()

        prv_reg = None
        prv_tok = None
        cur_tok = None
        valid_regs = __class__.valid_regs
        inv_valid_regs = __class__.inv_valid_regs

        while True:
            try:
                cur_tok = next(tg)
            except:
                raise RuntimeError("expected end of register list '}' before end of line")
            if cur_tok in valid_regs:
                if prv_tok is None or prv_tok is Special.COMMA:
                    self.register_set.add(cur_tok)
                elif prv_tok is Special.DASH:
                    self._add_range(prv_reg, cur_tok)
                else:
                    raise RuntimeError('cant parse register list - unexpected register ' + cur_tok);
                prv_reg = cur_tok
            elif cur_tok is Special.DASH:
                pass
            elif cur_tok is Special.COMMA:
                pass
            elif cur_tok is Special.RLIST_END:
                if prv_tok in valid_regs:
                    break
                else:
                    raise RuntimeError('unexpected end of token list.')
            else:
                raise RuntimeError('cant parse register list - unexpected token ' + cur_tok);
            prv_tok = cur_tok            

    def _add_range(self, r0, rn):
        index_0 = __class__.valid_regs[r0]
        index_n = __class__.valid_regs[rn]
        for i in range(index_0, index_n+1):
            r = __class__.inv_valid_regs[i]
            self.register_set.add(r)

    def __str__(self):
        s = "Register List: {"
        for r in self.register_set:
            s = s + r + ","
        s = s + "}"
        return s

def _chargen(line, state):
    """Character generator. Drop leading and duplicate spaces. Track quote and comment state."""
    for c in line:
        if state.in_comment:
            yield c
        elif state.in_squot:
            if c is "\'":
                state.in_squot = False
                yield Special.SQUOTE_END
            else:
                yield c
        elif state.in_dquot:
            if c is "\"":
                state.in_dquot = False
                yield Special.DQUOTE_END
            else:
                yield c
        elif c == ";":
            yield Special.COMMENT_BEG
            state.in_comment = True
        elif c == "\'":
            yield Special.SQUOTE_BEG
            state.in_squot = True
        elif c == "\"":
            yield Special.DQUOTE_BEG
            state.in_dquot = True
        elif c == "[":
            yield Special.ADDR_BEG
        elif c == "]":
            yield Special.ADDR_END
        elif c == "{":
            yield Special.RLIST_BEG
        elif c == "}":
            yield Special.RLIST_END
        elif c == '-':
            yield Special.DASH
        elif c == ',':
            yield Special.COMMA
        elif c == '+':
            yield Special.PLUS
        elif c == '=':
            yield Special.EQUAL
        elif c == ':':
            yield Special.COLON
        else:
            if not c.isspace():
                yield c
            elif not state.last_c is None:
                if not state.last_c.isspace():
                    yield c
        state.last_c = c

def _stringlit(line):
    state = State()
    cg = _chargen(line,state)
    for c in cg:
        if c is Special.COMMENT_BEG:
            yield Comment(cg)
        elif c is Special.SQUOTE_BEG:
            yield SQuote(cg)
        elif c is Special.DQUOTE_BEG:
            yield DQuote(cg)
        else:
            yield c

def _words(line):
    cg = _stringlit(line)
    split = (',')
    word = None
    for c in cg:
        if isinstance(c, StringLit):
            if not word is None:
                yield word
                word = None
            yield c
        elif type(c) is Special:
            if not word is None:
                yield word
                word = None
            yield c
        elif c.isspace():
            if not word is None:
                yield word
                word = None
        elif c in split:
            if not word is None:
                yield word
                word = None
            yield c
        elif word is None:
            word = c
        else:
            word = word + c
    if not word is None:
        yield word

def _numbers(line):
    tg = _words(line)
    for tok in tg:
        if isinstance(tok, str) and len(tok) > 0 and tok[0].isdigit():
            if len(tok) == 1:
                yield int(tok,10)
            elif len(tok) >= 2:
                if tok[1].lower() == 'x':
                    yield int(tok,16)
                elif tok[1].lower() == 'b':
                    yield int(tok,2)
                elif tok[0] == '0':
                    yield int(tok,8)
                else:
                    yield int(tok,10)
        else:
            yield tok

def _group(line):
    tg = _numbers(line)
    for t in tg:
        if t is Special.RLIST_BEG:
            yield RegisterList(tg)
        elif t is Special.ADDR_BEG:
            yield Address(tg)
        else:
            yield t

def _segaddress(line):
    registers = Registers.Registers()
    tg = _group(line)
    for t in tg:
        if registers.is_segment_mnemonic(t):
            try:
                n = next(tg)
                if type(n) is Address:
                    yield SegmentAddress(t,n)
                else:
                    yield t
                    yield n
            except:
                yield t
                raise StopIteration()
        else:
            yield t

class Instruction:
    def __init__(self, line):
        tg = _segaddress(line)
        self.operands = []
        self.instruction = next(tg)
        self.comment = None
        for t in tg:
            if t is not Special.COMMA:
                if type(t) is Comment:
                    self.comment = t
                    return
                else:
                    self.operands.append(t)

    def __str__(self):
        s = ""
        if self.comment is not None:
            s = s + str(self.comment) + "\n"
        if self.instruction is not None:
            s = s + "  " + str(self.instruction) + "\n"
        if len(self.operands) > 0:
            for op in self.operands:
                s = s + "    " + str(op) + "\n"
        return s
