"""
CPU Registers.

GENERAL PURPOSE: (r0, r1, r2, r3 ,r4):
General purpose registers are only accessed explicitly in instruction operands.

SPECIAL PURPOSE: (sp, lr, pc):

  The stack-pointer (sp) can be used as a general purpose register.
It is only special with regard to the push and pop instructions.
Any general purpose, or special purpose (EXCEPT SP) register
can be pushed onto, or popped off of the stack.
The stack growns down.

  The link-register (lr) can be used as a general purpose register.
It is only special with regard to the call instruction.
The call instruction sets the link-register to the address of the
next instruction before branching.

  The program-counter (pc) can be read from and written to in the same
way as a general purpose register. The value read from the PC is always
the address of the instruction after the presently executing instruction.
Writes to the pc cause the cpu to jump to that instruction.

ADDRESS SEGMENT: (as0, as1, as2, as3)

  The address segment registers and ONLY be read from or written to
using the mov instruction.
Theese registers can be used in conjunction with the ldr/str instructions.
When the specified register is 0, local memory regment is used.
1,2,3 and 4 select north, east, south and west segments.
"""


class Register():
    """Holds a registers data, and meta-data.
    """
    opcode_length = 3
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        self.value = 0

class SegmentRegister:
    opcode_length = 2
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        self.value = 0
        
class Registers:
    """Holds an instance of each register.
    """
    def __init__(self):
        self.r0  = Register(opcode='000', mnemonic='r0', list_idx = 0)
        self.r1  = Register(opcode='001', mnemonic='r1', list_idx = 1)
        self.r2  = Register(opcode='010', mnemonic='r2', list_idx = 2)
        self.r3  = Register(opcode='011', mnemonic='r3', list_idx = 3)
        self.r4  = Register(opcode='100', mnemonic='r4', list_idx = 4)
        self.sp  = Register(opcode='101', mnemonic='sp')
        self.lr  = Register(opcode='110', mnemonic='lr', list_idx = 5)
        self.pc  = Register(opcode='111', mnemonic='pc', list_idx = 6)
        
        self.as0 = SegmentRegister(opcode='00' , mnemonic='as0')
        self.as1 = SegmentRegister(opcode='01' , mnemonic='as1')
        self.as2 = SegmentRegister(opcode='10' , mnemonic='as2')
        self.as3 = SegmentRegister(opcode='11' , mnemonic='as3')

        self.general = self.r0, self.r1, self.r2, self.r3, self.r4
        self.special = self.sp, self.lr, self.pc
        self.segment = self.as0, self.as1, self.as2, self.as3
        self.all = self.general + self.special + self.segment
        self.mnemonics = { register.mnemonic: register for register in self.all }
        self.opcodes   = { register.opcode:   register for register in self.all }

    def is_segment_mnemonic(self, r):
        for s in self.segment:
            if s.mnemonic == r:
                return True
        return False
